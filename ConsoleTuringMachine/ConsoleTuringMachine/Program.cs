﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTuringMachine {
    using TM = TuringMachine;
    using Direction = TuringMachine.Direction;
    using Command = TuringMachine.Command;

    public class TuringMachine {

        public static char EmptyChar = (char)0;

        public enum Direction {
            Left = -1,
            Stay = 0,
            Right = 1
        }

        public class Command {
            private readonly char _inChar;
            public readonly State NextState;
            public readonly char OutChar;
            public readonly Direction Dir;

            public Command(char inChar, State nextState, char? outChar = null, Direction dir = Direction.Stay) {
                _inChar = inChar;
                NextState = nextState;
                OutChar = outChar ?? inChar;
                Dir = dir;
            }

            public Command(char inChar, State nextState, Direction dir) {
                _inChar = inChar;
                NextState = nextState;
                OutChar = inChar;
                Dir = dir;
            }

            public Command(char inChar, char? outChar = null, Direction dir = Direction.Stay) {
                _inChar = inChar;
                NextState = StopState;
                OutChar = outChar ?? inChar;
                Dir = dir;
            }

            public Command(char inChar, Direction dir) {
                _inChar = inChar;
                NextState = StopState;
                OutChar = inChar;
                Dir = dir;
            }

            public static bool operator ==(Command command, char c) {
                return command != null && command._inChar == c;
            }

            public static bool operator ==(char c, Command command) {
                return command == c;
            }

            public static bool operator !=(Command command, char c) {
                return !(command == c);
            }

            public static bool operator !=(char c, Command command) {
                return !(command == c);
            }
        }

        public class State {
            private static int _stateCount = 0;
            private readonly int _id;
            private readonly string _name;
            private readonly List<Command> _commands;

            public State(string name = null) {
                _id = _stateCount;
                _stateCount++;
                _name = name ?? _id.ToString();
                _commands = new List<Command>();
            }

            public override string ToString() {
                return $"State No.{_id}. Name: {_name}.";
            }

            public void AddCommand(Command command) {
                _commands.Add(command);
            }

            public Command GetCommand(char c) {
                foreach (var command in _commands) {
                    if (command == c) return command;
                }
                return EmptyCommand;
            }

            public static bool operator ==(State s1, State s2) {
                return s1._id == s2._id;
            }

            public static bool operator !=(State s1, State s2) {
                return !(s1 == s2);
            }
        }

        public static readonly State StopState = new State("END");
        private static readonly Command EmptyCommand = new Command(EmptyChar, Direction.Stay);

        private State _currentState;
        private readonly List<char> _sequence;
        private int _i;

        private void PrintSequence() {
            var i = 0;
            foreach (var c in _sequence) {
                if (_i == i) {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(c == EmptyChar ? '_' : c);
                    Console.ForegroundColor = ConsoleColor.Gray;
                } else
                    Console.Write(c == EmptyChar ? ' ' : c);
                i++;
            }
            Console.WriteLine();
        }

        public bool Step() {
            var c = _currentState.GetCommand(_sequence[_i]);
            if (c.Equals(EmptyCommand)) return false;
            _sequence[_i] = c.OutChar;
            _i += (int) c.Dir;
            _currentState = c.NextState;
            return _currentState != StopState;
        }

        public struct Settings {
            public bool PrintStateEachStep;
            public bool PrintSequenceEachStep;
            public bool RunImmediately;
            public bool ClearConsoleAfterIteration;
            public int Delay;//works only if not RunImmediately
        }

        private Settings _settings;

        public TuringMachine(int totalLen = 100, int startPlace = 30) {
            _sequence = new List<char>();
            for (var i = 0; i < totalLen; i++) {
                _sequence.Add((char) 0);
            }
            _i = startPlace < totalLen ? startPlace : totalLen / 3;
            _settings = new Settings {
                PrintStateEachStep = true,
                PrintSequenceEachStep = false,
                RunImmediately = true,
                ClearConsoleAfterIteration = false,
                Delay = 0
            };
        }

        public void SetSettings(Settings settings) {
            _settings = settings;
        }

        public void InitSequence(string s, int place = 0) {
            var i = 0;
            foreach (var c in s) {
                _sequence[_i + i] = c;
                i++;
            }
            _i += place;
        }

        public void Run(State initialState) {
            _currentState = initialState;
            Console.WriteLine("Initial sequence:");
            if (!_settings.PrintSequenceEachStep) PrintSequence();
            do {
                if (_settings.PrintStateEachStep)
                    Console.WriteLine(_currentState);
                if (_settings.PrintSequenceEachStep)
                    PrintSequence();
                if (!_settings.RunImmediately)
                    Console.ReadKey();
                else
                    if (_settings.Delay>0)
                        System.Threading.Thread.Sleep(_settings.Delay);
                if (_settings.ClearConsoleAfterIteration)
                    Console.Clear();
            } while (Step());
            Console.WriteLine("Final sequence:");
            PrintSequence();
            Console.ReadKey();
        }

    }

    class Program {

        private static char StrToChar(string s) {
            return new[] { "emp", "lam", "empty", "lambda" }.Contains(s.ToLower()) ? TM.EmptyChar : s[0];
        }
        private static Direction StrToDir(string s) {
            if (s == "<") return Direction.Left;
            if (s == ">") return Direction.Right;
            return Direction.Stay;
        }
        
        static void Main(string[] args) {
            var tm = new TuringMachine(160, 1);
            var faktTask = "||||!";
            Console.Write($"Write your faktorial task. Example: {faktTask}. > ");
            faktTask = Console.ReadLine();
            tm.InitSequence(faktTask, faktTask.Length-1);
            tm.SetSettings(new TM.Settings {
                PrintStateEachStep = true,
                PrintSequenceEachStep = true,
                RunImmediately = true,
                ClearConsoleAfterIteration = true,
                Delay = 50
            });
            var stateDict = new Dictionary<string, TM.State>();
            try {
                string[] lines = File.ReadAllLines(@"turing.csv", Encoding.UTF8);
                string[,] cells = new string[lines.Length, lines[0].Split(';').Length];
                var inChars = lines[0].Split(';');
                int j;
                for (var i = 1; i < lines.Length; i++) {
                    var tmp = lines[i].Split(';');
                    for (j = 0; j < tmp.Length; j++) {
                        cells[i, j] = tmp[j];
                    }
                    stateDict.Add(tmp[0], new TM.State(tmp[0]));
                }

                j = 1;
                foreach (var state in stateDict) {
                    for (var i = 1; i < inChars.Length; i++) {
                        if (cells[j,i].Length>0) {
                            var tmp = cells[j, i].Split(' ');
                            TM.State tmpSt;
                            if (stateDict.TryGetValue(tmp[0], out tmpSt)) {
                                state.Value.AddCommand(new Command(StrToChar(inChars[i]), tmpSt, StrToChar(tmp[1]), StrToDir(tmp[2])));
                            } else if (tmp[0] == "STOP") {
                                state.Value.AddCommand(new Command(StrToChar(inChars[i]), TM.StopState, StrToChar(tmp[1]), StrToDir(tmp[2])));
                            }
                        }
                    }
                    j++;
                }
                tm.Run(stateDict.First().Value);
                //Console.ReadKey();
            }
            catch (Exception e) {
                Console.WriteLine(e);
                Console.ReadKey();
                return;
            }

            //var initialState1 = new TM.State("init");  //  1
            //var initialState2 = new TM.State("init");  //  2
            //var initialState3 = new TM.State("init");  //  3
            //var initialState4 = new TM.State("init");  //  4
            //var step1_1 = new TM.State("step1");       //  5
            //var step1_2 = new TM.State("step1");       //  6
            //var step2_1 = new TM.State("step2");       //  7
            //var step2_2 = new TM.State("step2");       //  8
            //var step30_1 = new TM.State("step3.0");    //  9
            //var step30_2 = new TM.State("step3.0");    // 10
            //var step30_3 = new TM.State("step3.0");    // 11
            //var step31_1 = new TM.State("step3.1");    // 12
            //var step31_2 = new TM.State("step3.1");    // 13
            //var step31_3 = new TM.State("step3.1");    // 14
            //var step32 = new TM.State("step3.2");      // 15
            //var step40 = new TM.State("step4.0");      // 16
            //var step41 = new TM.State("step4.1");      // 17
            //var step42 = new TM.State("step4.2");      // 18
            //
            //initialState1.AddCommand(new Command('!', initialState1, Direction.Right));                //  1
            //initialState1.AddCommand(new Command(TM.EmptyChar, initialState2, '|', Direction.Left));   //  2
            //initialState2.AddCommand(new Command('!', initialState2, Direction.Left));                 //  3
            //initialState2.AddCommand(new Command('|', initialState2, Direction.Left));                 //  4
            //initialState2.AddCommand(new Command(TM.EmptyChar, initialState3, Direction.Right));       //  5
            //initialState3.AddCommand(new Command('|', initialState4, TM.EmptyChar, Direction.Right));  //  6
            //initialState3.AddCommand(new Command('!', step40, Direction.Left));                        //  7
            //initialState4.AddCommand(new Command('|', initialState4, Direction.Right));                //  8
            //initialState4.AddCommand(new Command('!', step1_1));                                       //  9
            //
            //step1_1.AddCommand(new Command('!', step1_1, Direction.Right));                            // 10
            //step1_1.AddCommand(new Command('i', step1_1, Direction.Right));                            // 11
            //step1_1.AddCommand(new Command('|', step1_2));                                             // 12
            //step1_2.AddCommand(new Command('|', step1_2, 'i', Direction.Right));                       // 13
            //step1_2.AddCommand(new Command(TM.EmptyChar, step1_2, Direction.Left));                    // 14
            //step1_2.AddCommand(new Command('i', step1_2, Direction.Left));                             // 15
            //step1_2.AddCommand(new Command('!', step2_1));                                             // 16
            //
            //step2_1.AddCommand(new Command('!', step2_1, Direction.Left));                             // 17
            //step2_1.AddCommand(new Command('1', step2_1, '0', Direction.Left));                        // 18
            //step2_1.AddCommand(new Command('|', step2_2, '0'));                                        // 19
            //step2_1.AddCommand(new Command(TM.EmptyChar, step40, '1', Direction.Left));                // 20
            //step2_2.AddCommand(new Command('0', step2_2, Direction.Right));                            // 21
            //step2_2.AddCommand(new Command('!', step30_1));                                            // 22
            //
            //step30_1.AddCommand(new Command('!', step30_1, Direction.Left));                           // 23
            //step30_1.AddCommand(new Command('1', step30_1, Direction.Left));                           // 24
            //step30_1.AddCommand(new Command('0', step30_2, '1'));                                      // 25
            //step30_1.AddCommand(new Command('|', step30_3, Direction.Right));                          // 26
            //step30_1.AddCommand(new Command(TM.EmptyChar, step40, Direction.Left));                    // 27
            //step30_2.AddCommand(new Command('1', step30_2, Direction.Right));                          // 28
            //step30_2.AddCommand(new Command('!', step31_1));                                           // 29
            //step30_3.AddCommand(new Command('1', step30_3, Direction.Right));                          // 30
            //step30_3.AddCommand(new Command('!', step1_1));                                            // 31
            //
            //step31_1.AddCommand(new Command('!', step31_1, Direction.Right));                          // 32
            //step31_1.AddCommand(new Command('i', step31_2, 'j', Direction.Right));                     // 33
            //step31_1.AddCommand(new Command('|', step32, Direction.Left));                             // 34
            //step31_2.AddCommand(new Command('i', step31_2, Direction.Right));                          // 35
            //step31_2.AddCommand(new Command('|', step31_2, Direction.Right));                          // 36
            //step31_2.AddCommand(new Command(TM.EmptyChar, step31_3, '|'));                             // 37
            //step31_3.AddCommand(new Command('|', step31_3, Direction.Left));                           // 38
            //step31_3.AddCommand(new Command('i', step31_3, Direction.Left));                           // 39
            //step31_3.AddCommand(new Command('j', step31_1, Direction.Right));                          // 40
            //
            //step32.AddCommand(new Command('j', step32, 'i', Direction.Left));                          // 41
            //step32.AddCommand(new Command('!', step30_1));                                             // 42
            //
            //step40.AddCommand(new Command(TM.EmptyChar, step40, '|', Direction.Right));                // 43
            //step40.AddCommand(new Command('1', step40, '|', Direction.Right));                         // 44
            //step40.AddCommand(new Command('!', step41));                                               // 45
            //
            //step41.AddCommand(new Command('!', step41, '=', Direction.Left));                          // 46
            //step41.AddCommand(new Command('|', step41, '!', Direction.Right));                         // 47
            //step41.AddCommand(new Command('=', step42, Direction.Right));                              // 48
            //
            //step42.AddCommand(new Command('i', step42, '|', Direction.Right));                         // 49
            //step42.AddCommand(new Command('|', step42, Direction.Left));                               // 50
            //step42.AddCommand(new Command(TM.EmptyChar, step42, Direction.Left));                      // 51
            //step42.AddCommand(new Command('=', TM.StopState));                                         // 52
            //
            //tm.Run(initialState1);
        }
    }
}
