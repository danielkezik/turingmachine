﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsTuringMachine {
    using TM = TuringMachine;
    using Direction = TuringMachine.Direction;
    using Command = TuringMachine.Command;

    public partial class TuringMachineForm : Form {

        public readonly TuringMachine Tm = new TuringMachine(1000,200);
        private int _startPrintPos = 200;
        public bool FollowCursor { get; set; }
        public bool MoveForm { get; set; }
        private SettingsForm _settingsForm;
        private readonly List<TextBox> _textBoxes;

        public TuringMachineForm() {
            InitializeComponent();
            _textBoxes = new List<TextBox> { TB0, TB1, TB2, TB3, TB4, TB5, TB6, TB7, TB8, TB9 };
        }

        private void TuringMachineForm_Load(object sender, EventArgs e) {
            Tm.PrintSequence = PrintSequence;
            Tm.SetSettings(new TM.Settings {
                RunAutomatically = true,
                Delay = 200
            });
            _settingsForm = new SettingsForm();
            FollowCursor = true;
            arrowController.Focus();
            foreach (var textBox in _textBoxes) {
                textBox.GotFocus += (o, args) => { arrowController.Focus(); };
            }
        }

        public void PrintSequence() {
            if (Tm.IsInProcess() && FollowCursor) {
                if (Tm.Pos < _startPrintPos) {
                    _startPrintPos = Tm.Pos;
                    if (MoveForm) Location = new Point(Location.X - 45, Location.Y);
                }
                if (Tm.Pos > _startPrintPos + 9) {
                    _startPrintPos = Tm.Pos - 9;
                    if (MoveForm) Location = new Point(Location.X + 45, Location.Y);
                }
            }
            var tmp = Tm.GetSequence(_startPrintPos);
            for (var i = 0; i < 10; i++) {
                _textBoxes[i].Text = CharToStr(tmp[i]);
                _textBoxes[i].Font = new Font(_textBoxes[i].Font, i + _startPrintPos == Tm.Pos ? FontStyle.Underline : FontStyle.Regular);
                _textBoxes[i].Refresh();
            }
        }

        private void openFileButton_Click(object sender, EventArgs e) {
            openCsvFileDialog.ShowDialog();
            arrowController.Focus();
        }

        private void ShiftViewLeftButton_Click(object sender, EventArgs e) {
            _startPrintPos--;
            if (MoveForm) Location = new Point(Location.X - 45, Location.Y);
            Tm.PrintSequence();
            arrowController.Focus();
        }

        private void ShiftViewRightButton_Click(object sender, EventArgs e) {
            _startPrintPos++;
            if (MoveForm) Location = new Point(Location.X + 45, Location.Y);
            Tm.PrintSequence();
            arrowController.Focus();
        }

        private void setInputButton_Click(object sender, EventArgs e) {
            Tm.InitSequence(initialSequenceTextBox.Text);//, initialSequenceTextBox.Text.Length-1);
            Tm.PrintSequence();
            arrowController.Focus();
        }

        private void settingButton_Click(object sender, EventArgs e) {
            _settingsForm.ShowDialog(this);
        }

        private void SetCellsEnabled(bool s) {
            foreach (var textBox in _textBoxes) {
                textBox.Enabled = s;
            }
        }

        private void SetButtonsEnabled(bool s) {
            runButton.Enabled = s;
            ShiftViewLeftButton.Enabled = s;
            ShiftViewRightButton.Enabled = s;
            SetCellsEnabled(s);
        }

        private void SetSettingsEnabled(bool s) {
            _settingsForm.runAutomaticallyCB.Enabled = s;
            _settingsForm.delayNumUpDown.Enabled = s;
            _settingsForm.delayLabel.Enabled = s;
        }

        private TM.State _initialState;
        private void runButton_Click(object sender, EventArgs e) {
            if (Tm.IsInProcess()) {
                if (!Tm.Step()) {
                    SetCellsEnabled(true);
                    SetSettingsEnabled(true);
                }
                Tm.PrintSequence();
            } else {
                if (Tm.GetSettings().RunAutomatically) SetButtonsEnabled(false);
                else SetCellsEnabled(false);
                SetSettingsEnabled(false);
                Tm.Run(_initialState);
                if (!Tm.IsInProcess()) {
                    SetButtonsEnabled(true);
                    SetSettingsEnabled(true);
                }
            }
            runButton.Text = Tm.IsInProcess() ? "Next" : "Run";
            if (!Tm.IsInProcess()) arrowController.Focus();
        }

        private static string CharToStr(char c) {
            return c == TM.EmptyChar ? " " : c.ToString();
        }

        private static char StrToChar(string s) {
            return new[] { "emp", "lam", "empty", "lambda" }.Contains(s.ToLower()) ? TM.EmptyChar : s[0];
        }

        private static Direction StrToDir(string s) {
            if (s == "<") return Direction.Left;
            if (s == ">") return Direction.Right;
            return Direction.Stay;
        }

        private void openCsvFileDialog_FileOk(object sender, CancelEventArgs e) {
            fileNameLabel.Text = openCsvFileDialog.FileName.Split(Path.DirectorySeparatorChar).Last();
            var stateDict = new Dictionary<string, TM.State>();
            try {
                string[] lines = File.ReadAllLines(openCsvFileDialog.FileName, Encoding.UTF8);
                string[,] cells = new string[lines.Length, lines[0].Split(';').Length];
                var inChars = lines[0].Split(';');
                int j;
                for (var i = 1; i < lines.Length; i++) {
                    var tmp = lines[i].Split(';');
                    for (j = 0; j < tmp.Length; j++) {
                        cells[i, j] = tmp[j];
                    }
                    stateDict.Add(tmp[0], new TM.State(tmp[0]));
                }

                j = 1;
                foreach (var state in stateDict) {
                    for (var i = 1; i < inChars.Length; i++) {
                        if (cells[j, i].Length > 0) {
                            var tmp = cells[j, i].Split(' ');
                            TM.State tmpSt;
                            if (stateDict.TryGetValue(tmp[0], out tmpSt)) {
                                state.Value.AddCommand(new Command(StrToChar(inChars[i]), tmpSt, StrToChar(tmp[1]), StrToDir(tmp[2])));
                            } else if (tmp[0] == "STOP") {
                                state.Value.AddCommand(new Command(StrToChar(inChars[i]), TM.StopState, StrToChar(tmp[1]), StrToDir(tmp[2])));
                            }
                        }
                    }
                    j++;
                }
                _initialState = stateDict.First().Value;
                runButton.Enabled = true;
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            arrowController.Focus();
        }

        private void TB_Click(object sender, EventArgs e) {
            Tm.Pos = _startPrintPos + int.Parse(((TextBox)sender).Name.Remove(0, 2));
            Tm.PrintSequence();
            arrowController.Focus();
        }

        private void TuringMachineForm_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Right) { ShiftViewRightButton_Click(sender,null); }
            if (e.KeyCode == Keys.Left) { ShiftViewLeftButton_Click(sender,null); }
        }

        private void initialSequenceTextBox_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                setInputButton_Click(null, null);
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }
    }
}
