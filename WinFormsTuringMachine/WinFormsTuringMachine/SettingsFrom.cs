﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsTuringMachine {
    public partial class SettingsForm : Form {
        public SettingsForm() {
            InitializeComponent();
            delayLabel.DataBindings.Add(new Binding("Enabled", runAutomaticallyCB, "Checked"));
            delayNumUpDown.DataBindings.Add(new Binding("Enabled", runAutomaticallyCB, "Checked"));
        }

        private void saveButton_Click(object sender, EventArgs e) {
            var o = (TuringMachineForm)Owner;
            o.FollowCursor=followCursorCB.Checked;
            var s = o.Tm.GetSettings();
            if (runAutomaticallyCB.Enabled && (s.RunAutomatically != runAutomaticallyCB.Checked || runAutomaticallyCB.Checked && delayNumUpDown.Value != s.Delay)) {
                o.Tm.SetSettings(new TuringMachine.Settings {
                    Delay = (int) delayNumUpDown.Value,
                    RunAutomatically = runAutomaticallyCB.Checked
                });
            }
            o.MoveForm = moveFormCB.Checked;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e) {
            Close();
        }

        private void SettingsForm_Load(object sender, EventArgs e) {
            var o = (TuringMachineForm)Owner;
            followCursorCB.Checked = o.FollowCursor;
            var s = o.Tm.GetSettings();
            runAutomaticallyCB.Checked = s.RunAutomatically;
            delayNumUpDown.Value = s.Delay;
            moveFormCB.Checked = o.MoveForm;
        }
    }
}
