﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsTuringMachine {
    public delegate void PrintSequenceDelegate();
    public class TuringMachine {

        public static char EmptyChar = (char)0;

        public enum Direction {
            Left = -1,
            Stay = 0,
            Right = 1
        }

        public class Command {
            private readonly char _inChar;
            public readonly State NextState;
            public readonly char OutChar;
            public readonly Direction Dir;

            public Command(char inChar, State nextState, char? outChar = null, Direction dir = Direction.Stay) {
                _inChar = inChar;
                NextState = nextState;
                OutChar = outChar ?? inChar;
                Dir = dir;
            }

            public Command(char inChar, State nextState, Direction dir) {
                _inChar = inChar;
                NextState = nextState;
                OutChar = inChar;
                Dir = dir;
            }

            public Command(char inChar, char? outChar = null, Direction dir = Direction.Stay) {
                _inChar = inChar;
                NextState = StopState;
                OutChar = outChar ?? inChar;
                Dir = dir;
            }

            public Command(char inChar, Direction dir) {
                _inChar = inChar;
                NextState = StopState;
                OutChar = inChar;
                Dir = dir;
            }

            public static bool operator ==(Command command, char c) {
                return command != null && command._inChar == c;
            }

            public static bool operator ==(char c, Command command) {
                return command == c;
            }

            public static bool operator !=(Command command, char c) {
                return !(command == c);
            }

            public static bool operator !=(char c, Command command) {
                return !(command == c);
            }
        }

        public class State {
            private static int _stateCount = 0;
            private readonly int _id;
            private readonly string _name;
            private readonly List<Command> _commands;

            public State(string name = null) {
                _id = _stateCount;
                _stateCount++;
                _name = name ?? _id.ToString();
                _commands = new List<Command>();
            }

            public override string ToString() {
                return $"State No.{_id}. Name: {_name}.";
            }

            public void AddCommand(Command command) {
                _commands.Add(command);
            }

            public Command GetCommand(char c) {
                foreach (var command in _commands) {
                    if (command == c) return command;
                }
                return EmptyCommand;
            }

            public static bool operator ==(State s1, State s2) {
                return s1._id == s2._id;
            }

            public static bool operator !=(State s1, State s2) {
                return !(s1 == s2);
            }
        }

        public static readonly State StopState = new State("END");
        private static readonly Command EmptyCommand = new Command(EmptyChar, Direction.Stay);

        private State _currentState = StopState;
        private readonly List<char> _sequence;
        public int Pos { get; set; }

        public List<char> GetSequence(int s, int count = 10) {
            var tmp = new List<char>();
            for (var i = s; i < s + count; i++)
                tmp.Add(_sequence[i]);
            return tmp;
        }

        //private void PrintSequence() {
        //    var i = 0;
        //    foreach (var c in _sequence) {
        //        if (Pos == i) {
        //            Console.ForegroundColor = ConsoleColor.Yellow;
        //            Console.Write(c == EmptyChar ? '_' : c);
        //            Console.ForegroundColor = ConsoleColor.Gray;
        //        } else
        //            Console.Write(c == EmptyChar ? ' ' : c);
        //        i++;
        //    }
        //    Console.WriteLine();
        //}
        public PrintSequenceDelegate PrintSequence;

        public bool Step() {
            var c = _currentState.GetCommand(_sequence[Pos]);
            if (c.Equals(EmptyCommand)) {
                _currentState = StopState;
                return false;
            }
            _sequence[Pos] = c.OutChar;
            Pos += (int)c.Dir;
            _currentState = c.NextState;
            return _currentState != StopState;
        }

        public struct Settings {
            public bool RunAutomatically;
            public int Delay;//works only if not RunImmediately
        }

        private Settings _settings;

        public TuringMachine(int totalLen = 100, int startPlace = 30) {
            _sequence = new List<char>();
            for (var i = 0; i < totalLen; i++) {
                _sequence.Add(EmptyChar);
            }
            Pos = startPlace < totalLen ? startPlace : totalLen / 3;
            _settings = new Settings {
                RunAutomatically = true,
                Delay = 0
            };
        }

        public void SetSettings(Settings settings) {
            _settings = settings;
        }

        public Settings GetSettings() {
            return _settings;
        }

        public void InitSequence(string s, int place = 0) {
            for (var j = 0; j < _sequence.Count; j++) {
                _sequence[j] = EmptyChar;
            }
            var i = 0;
            foreach (var c in s) {
                _sequence[Pos + i] = c;
                i++;
            }
            Pos += place;
        }

        private bool _isInProcess = false;

        public void Run(State initialState) {
            _currentState = initialState;
            if (_settings.RunAutomatically) {
                _isInProcess = true;
                do {
                    if (_settings.Delay > 0) {
                        PrintSequence();
                        System.Threading.Thread.Sleep(_settings.Delay);
                    }
                } while (Step());
            } else {
                Step();
            }
            PrintSequence();
            if (_settings.RunAutomatically) { _isInProcess = false; }
        }

        public bool IsInProcess() {
            return (_currentState != StopState) || _isInProcess;
        }

    }
}
