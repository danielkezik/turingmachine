﻿namespace WinFormsTuringMachine {
    partial class SettingsForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.delayLabel = new System.Windows.Forms.Label();
            this.followCursorCB = new System.Windows.Forms.CheckBox();
            this.runAutomaticallyCB = new System.Windows.Forms.CheckBox();
            this.delayNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.moveFormCB = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.delayNumUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // delayLabel
            // 
            this.delayLabel.AutoSize = true;
            this.delayLabel.Location = new System.Drawing.Point(95, 84);
            this.delayLabel.Name = "delayLabel";
            this.delayLabel.Size = new System.Drawing.Size(56, 13);
            this.delayLabel.TabIndex = 2;
            this.delayLabel.Text = "Delay (ms)";
            // 
            // followCursorCB
            // 
            this.followCursorCB.AutoSize = true;
            this.followCursorCB.Checked = true;
            this.followCursorCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.followCursorCB.Location = new System.Drawing.Point(20, 12);
            this.followCursorCB.Name = "followCursorCB";
            this.followCursorCB.Size = new System.Drawing.Size(153, 17);
            this.followCursorCB.TabIndex = 3;
            this.followCursorCB.Text = "Follow cursor while running";
            this.followCursorCB.UseVisualStyleBackColor = true;
            // 
            // runAutomaticallyCB
            // 
            this.runAutomaticallyCB.AutoSize = true;
            this.runAutomaticallyCB.Checked = true;
            this.runAutomaticallyCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.runAutomaticallyCB.Location = new System.Drawing.Point(20, 60);
            this.runAutomaticallyCB.Name = "runAutomaticallyCB";
            this.runAutomaticallyCB.Size = new System.Drawing.Size(110, 17);
            this.runAutomaticallyCB.TabIndex = 4;
            this.runAutomaticallyCB.Text = "Run automatically";
            this.runAutomaticallyCB.UseVisualStyleBackColor = true;
            // 
            // delayNumUpDown
            // 
            this.delayNumUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.delayNumUpDown.Location = new System.Drawing.Point(20, 82);
            this.delayNumUpDown.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.delayNumUpDown.Name = "delayNumUpDown";
            this.delayNumUpDown.Size = new System.Drawing.Size(69, 20);
            this.delayNumUpDown.TabIndex = 5;
            this.delayNumUpDown.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(180, 12);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(92, 23);
            this.saveButton.TabIndex = 6;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(180, 41);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(92, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // moveFormCB
            // 
            this.moveFormCB.AutoSize = true;
            this.moveFormCB.Location = new System.Drawing.Point(20, 36);
            this.moveFormCB.Name = "moveFormCB";
            this.moveFormCB.Size = new System.Drawing.Size(76, 17);
            this.moveFormCB.TabIndex = 8;
            this.moveFormCB.Text = "Move form";
            this.moveFormCB.UseVisualStyleBackColor = true;
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.saveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(284, 114);
            this.Controls.Add(this.moveFormCB);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.delayNumUpDown);
            this.Controls.Add(this.runAutomaticallyCB);
            this.Controls.Add(this.followCursorCB);
            this.Controls.Add(this.delayLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.delayNumUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        public System.Windows.Forms.Label delayLabel;
        public System.Windows.Forms.CheckBox followCursorCB;
        public System.Windows.Forms.CheckBox runAutomaticallyCB;
        public System.Windows.Forms.NumericUpDown delayNumUpDown;
        private System.Windows.Forms.CheckBox moveFormCB;
    }
}