﻿namespace WinFormsTuringMachine {
    partial class TuringMachineForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TuringMachineForm));
            this.openFileButton = new System.Windows.Forms.Button();
            this.openCsvFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.TB0 = new System.Windows.Forms.TextBox();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.TB1 = new System.Windows.Forms.TextBox();
            this.TB2 = new System.Windows.Forms.TextBox();
            this.TB3 = new System.Windows.Forms.TextBox();
            this.TB4 = new System.Windows.Forms.TextBox();
            this.TB5 = new System.Windows.Forms.TextBox();
            this.TB6 = new System.Windows.Forms.TextBox();
            this.TB7 = new System.Windows.Forms.TextBox();
            this.TB8 = new System.Windows.Forms.TextBox();
            this.TB9 = new System.Windows.Forms.TextBox();
            this.initialSequenceTextBox = new System.Windows.Forms.TextBox();
            this.settingButton = new System.Windows.Forms.Button();
            this.setInputButton = new System.Windows.Forms.Button();
            this.ShiftViewLeftButton = new System.Windows.Forms.Button();
            this.ShiftViewRightButton = new System.Windows.Forms.Button();
            this.runButton = new System.Windows.Forms.Button();
            this.arrowController = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // openFileButton
            // 
            this.openFileButton.Location = new System.Drawing.Point(12, 28);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.Size = new System.Drawing.Size(111, 23);
            this.openFileButton.TabIndex = 2;
            this.openFileButton.Text = "Open file";
            this.openFileButton.UseVisualStyleBackColor = true;
            this.openFileButton.Click += new System.EventHandler(this.openFileButton_Click);
            // 
            // openCsvFileDialog
            // 
            this.openCsvFileDialog.FileName = "turing.csv";
            this.openCsvFileDialog.Filter = "CSV files (*.csv)|*.csv";
            this.openCsvFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openCsvFileDialog_FileOk);
            // 
            // TB0
            // 
            this.TB0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TB0.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB0.HideSelection = false;
            this.TB0.Location = new System.Drawing.Point(175, 12);
            this.TB0.MaxLength = 10;
            this.TB0.Name = "TB0";
            this.TB0.ReadOnly = true;
            this.TB0.Size = new System.Drawing.Size(39, 39);
            this.TB0.TabIndex = 9;
            this.TB0.TabStop = false;
            this.TB0.Text = " ";
            this.TB0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TB0.Click += new System.EventHandler(this.TB_Click);
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.Location = new System.Drawing.Point(12, 12);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(111, 13);
            this.fileNameLabel.TabIndex = 1;
            this.fileNameLabel.Text = "File Name";
            this.fileNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TB1
            // 
            this.TB1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TB1.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB1.HideSelection = false;
            this.TB1.Location = new System.Drawing.Point(220, 12);
            this.TB1.MaxLength = 10;
            this.TB1.Name = "TB1";
            this.TB1.ReadOnly = true;
            this.TB1.Size = new System.Drawing.Size(39, 39);
            this.TB1.TabIndex = 10;
            this.TB1.TabStop = false;
            this.TB1.Text = " ";
            this.TB1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TB1.Click += new System.EventHandler(this.TB_Click);
            // 
            // TB2
            // 
            this.TB2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TB2.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB2.HideSelection = false;
            this.TB2.Location = new System.Drawing.Point(265, 12);
            this.TB2.MaxLength = 10;
            this.TB2.Name = "TB2";
            this.TB2.ReadOnly = true;
            this.TB2.Size = new System.Drawing.Size(39, 39);
            this.TB2.TabIndex = 11;
            this.TB2.TabStop = false;
            this.TB2.Text = " ";
            this.TB2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TB2.Click += new System.EventHandler(this.TB_Click);
            // 
            // TB3
            // 
            this.TB3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TB3.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB3.HideSelection = false;
            this.TB3.Location = new System.Drawing.Point(310, 12);
            this.TB3.MaxLength = 10;
            this.TB3.Name = "TB3";
            this.TB3.ReadOnly = true;
            this.TB3.Size = new System.Drawing.Size(39, 39);
            this.TB3.TabIndex = 12;
            this.TB3.TabStop = false;
            this.TB3.Text = " ";
            this.TB3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TB3.Click += new System.EventHandler(this.TB_Click);
            // 
            // TB4
            // 
            this.TB4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TB4.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB4.HideSelection = false;
            this.TB4.Location = new System.Drawing.Point(355, 12);
            this.TB4.MaxLength = 10;
            this.TB4.Name = "TB4";
            this.TB4.ReadOnly = true;
            this.TB4.Size = new System.Drawing.Size(39, 39);
            this.TB4.TabIndex = 13;
            this.TB4.TabStop = false;
            this.TB4.Text = " ";
            this.TB4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TB4.Click += new System.EventHandler(this.TB_Click);
            // 
            // TB5
            // 
            this.TB5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TB5.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB5.HideSelection = false;
            this.TB5.Location = new System.Drawing.Point(400, 12);
            this.TB5.MaxLength = 10;
            this.TB5.Name = "TB5";
            this.TB5.ReadOnly = true;
            this.TB5.Size = new System.Drawing.Size(39, 39);
            this.TB5.TabIndex = 14;
            this.TB5.TabStop = false;
            this.TB5.Text = " ";
            this.TB5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TB5.Click += new System.EventHandler(this.TB_Click);
            // 
            // TB6
            // 
            this.TB6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TB6.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB6.HideSelection = false;
            this.TB6.Location = new System.Drawing.Point(445, 12);
            this.TB6.MaxLength = 10;
            this.TB6.Name = "TB6";
            this.TB6.ReadOnly = true;
            this.TB6.Size = new System.Drawing.Size(39, 39);
            this.TB6.TabIndex = 15;
            this.TB6.TabStop = false;
            this.TB6.Text = " ";
            this.TB6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TB6.Click += new System.EventHandler(this.TB_Click);
            // 
            // TB7
            // 
            this.TB7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TB7.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB7.HideSelection = false;
            this.TB7.Location = new System.Drawing.Point(490, 12);
            this.TB7.MaxLength = 10;
            this.TB7.Name = "TB7";
            this.TB7.ReadOnly = true;
            this.TB7.Size = new System.Drawing.Size(39, 39);
            this.TB7.TabIndex = 16;
            this.TB7.TabStop = false;
            this.TB7.Text = " ";
            this.TB7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TB7.Click += new System.EventHandler(this.TB_Click);
            // 
            // TB8
            // 
            this.TB8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TB8.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB8.HideSelection = false;
            this.TB8.Location = new System.Drawing.Point(535, 12);
            this.TB8.MaxLength = 10;
            this.TB8.Name = "TB8";
            this.TB8.ReadOnly = true;
            this.TB8.Size = new System.Drawing.Size(39, 39);
            this.TB8.TabIndex = 17;
            this.TB8.TabStop = false;
            this.TB8.Text = " ";
            this.TB8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TB8.Click += new System.EventHandler(this.TB_Click);
            // 
            // TB9
            // 
            this.TB9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TB9.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB9.HideSelection = false;
            this.TB9.Location = new System.Drawing.Point(580, 12);
            this.TB9.MaxLength = 10;
            this.TB9.Name = "TB9";
            this.TB9.ReadOnly = true;
            this.TB9.Size = new System.Drawing.Size(39, 39);
            this.TB9.TabIndex = 18;
            this.TB9.TabStop = false;
            this.TB9.Text = " ";
            this.TB9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TB9.Click += new System.EventHandler(this.TB_Click);
            // 
            // initialSequenceTextBox
            // 
            this.initialSequenceTextBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.initialSequenceTextBox.Location = new System.Drawing.Point(670, 12);
            this.initialSequenceTextBox.Name = "initialSequenceTextBox";
            this.initialSequenceTextBox.Size = new System.Drawing.Size(100, 20);
            this.initialSequenceTextBox.TabIndex = 5;
            this.initialSequenceTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.initialSequenceTextBox_KeyDown);
            // 
            // settingButton
            // 
            this.settingButton.Location = new System.Drawing.Point(776, 11);
            this.settingButton.Name = "settingButton";
            this.settingButton.Size = new System.Drawing.Size(75, 21);
            this.settingButton.TabIndex = 7;
            this.settingButton.Text = "Settings";
            this.settingButton.UseVisualStyleBackColor = true;
            this.settingButton.Click += new System.EventHandler(this.settingButton_Click);
            // 
            // setInputButton
            // 
            this.setInputButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.setInputButton.Location = new System.Drawing.Point(670, 31);
            this.setInputButton.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.setInputButton.Name = "setInputButton";
            this.setInputButton.Size = new System.Drawing.Size(100, 21);
            this.setInputButton.TabIndex = 6;
            this.setInputButton.Text = "Set";
            this.setInputButton.UseVisualStyleBackColor = true;
            this.setInputButton.Click += new System.EventHandler(this.setInputButton_Click);
            // 
            // ShiftViewLeftButton
            // 
            this.ShiftViewLeftButton.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ShiftViewLeftButton.Location = new System.Drawing.Point(129, 11);
            this.ShiftViewLeftButton.Name = "ShiftViewLeftButton";
            this.ShiftViewLeftButton.Size = new System.Drawing.Size(41, 41);
            this.ShiftViewLeftButton.TabIndex = 3;
            this.ShiftViewLeftButton.Text = "<";
            this.ShiftViewLeftButton.UseVisualStyleBackColor = true;
            this.ShiftViewLeftButton.Click += new System.EventHandler(this.ShiftViewLeftButton_Click);
            // 
            // ShiftViewRightButton
            // 
            this.ShiftViewRightButton.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ShiftViewRightButton.Location = new System.Drawing.Point(625, 11);
            this.ShiftViewRightButton.Name = "ShiftViewRightButton";
            this.ShiftViewRightButton.Size = new System.Drawing.Size(41, 41);
            this.ShiftViewRightButton.TabIndex = 4;
            this.ShiftViewRightButton.Text = ">";
            this.ShiftViewRightButton.UseVisualStyleBackColor = true;
            this.ShiftViewRightButton.Click += new System.EventHandler(this.ShiftViewRightButton_Click);
            // 
            // runButton
            // 
            this.runButton.Enabled = false;
            this.runButton.Location = new System.Drawing.Point(776, 31);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(75, 23);
            this.runButton.TabIndex = 8;
            this.runButton.Text = "RUN";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            this.runButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TuringMachineForm_KeyDown);
            // 
            // arrowController
            // 
            this.arrowController.Location = new System.Drawing.Point(-15, 0);
            this.arrowController.Name = "arrowController";
            this.arrowController.ReadOnly = true;
            this.arrowController.Size = new System.Drawing.Size(10, 20);
            this.arrowController.TabIndex = 0;
            this.arrowController.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TuringMachineForm_KeyDown);
            // 
            // TuringMachineForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 62);
            this.Controls.Add(this.arrowController);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.ShiftViewRightButton);
            this.Controls.Add(this.ShiftViewLeftButton);
            this.Controls.Add(this.setInputButton);
            this.Controls.Add(this.settingButton);
            this.Controls.Add(this.initialSequenceTextBox);
            this.Controls.Add(this.TB9);
            this.Controls.Add(this.TB8);
            this.Controls.Add(this.TB7);
            this.Controls.Add(this.TB6);
            this.Controls.Add(this.TB5);
            this.Controls.Add(this.TB4);
            this.Controls.Add(this.TB3);
            this.Controls.Add(this.TB2);
            this.Controls.Add(this.TB1);
            this.Controls.Add(this.fileNameLabel);
            this.Controls.Add(this.TB0);
            this.Controls.Add(this.openFileButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "TuringMachineForm";
            this.Text = "Turing Machine";
            this.Load += new System.EventHandler(this.TuringMachineForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openFileButton;
        private System.Windows.Forms.OpenFileDialog openCsvFileDialog;
        private System.Windows.Forms.TextBox TB0;
        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.TextBox TB1;
        private System.Windows.Forms.TextBox TB2;
        private System.Windows.Forms.TextBox TB3;
        private System.Windows.Forms.TextBox TB4;
        private System.Windows.Forms.TextBox TB5;
        private System.Windows.Forms.TextBox TB6;
        private System.Windows.Forms.TextBox TB7;
        private System.Windows.Forms.TextBox TB8;
        private System.Windows.Forms.TextBox TB9;
        private System.Windows.Forms.TextBox initialSequenceTextBox;
        private System.Windows.Forms.Button settingButton;
        private System.Windows.Forms.Button setInputButton;
        private System.Windows.Forms.Button ShiftViewLeftButton;
        private System.Windows.Forms.Button ShiftViewRightButton;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.TextBox arrowController;
    }
}

